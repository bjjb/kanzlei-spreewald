Kanzlei Spreewald
=================

(formerly Anwaltzkanzlei Schenk)

A Sintra / static website.

To build, install Ruby, RubyGems and Bundler, then run

    bundle
    bundle exec rake

To test locally, run

    bundle rackup

To deploy, push to Heroku (git@heroku.com:kanzlei-spreewald.git) master.

## To do

- [ ] Modernise build system
    - [ ] Ruby 3
    - [ ] Modern CSS

---

© Kristin Bergmann, JJ Buckley, all rights reserved.
