terraform {
  required_version = "~> 1.7.1"

  # Variables for CI are stored as vars on GitLab project 41359276,
  # variables for local use are in .envrc.
  backend "http" {}

  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
    }
  }
}

provider "cloudflare" {
  # CLOUDFLARE_API_TOKEN must be set!
}

resource "cloudflare_zone" "primary" {
  zone       = "kanzlei-spreewald.de"
  account_id = "739adf185268cc371b492d1b440069fe"
}

resource "cloudflare_record" "apex" {
  zone_id = cloudflare_zone.primary.id
  name    = "kanzlei-spreewald.de"
  type    = "A"
  value   = "35.185.44.232" # projects.gitlab.io
  comment = "Points to pages.gitlab.io"
  # TODO - see if a CNAME would work here instead.
}

resource "cloudflare_record" "www" {
  zone_id = cloudflare_zone.primary.id
  name    = "www"
  type    = "CNAME"
  value   = cloudflare_record.apex.name
  proxied = true
}

# TODO: This is a bit inconsistend and unwieldy. Just create each record
# individually.
locals {
  dns = {
    txt = {
      google = {
        name    = cloudflare_record.apex.name
        value   = "google-site-verification=4wRsefPzd9IAsXicA2ohxb3f8wYoqDTLSc7ton8iaP8"
        comment = "Google domain verification"
      },
      gitlab = {
        name    = cloudflare_record.apex.name
        value   = "_gitlab-pages-verification-code.kanzlei-spreewald.de TXT gitlab-pages-verification-code=202b7c48405eadbb349e5c28e8b588c7"
        comment = "GitLab pages verification"
      }
      dkim = {
        name    = "google._domainkey"
        value   = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAocxX1m3q5Kqxk8BI5Nr7bEfqA6m0jczhajgyrrQgk6C5em4y3/qQyeg2jT6JteQVP9tzVa/VlENOGA4qttq8VueL90aLmzxfhBRHQjKmlraUzXrzXi+7o+Dg4Z254T1tMqmqp2dU66AGDDO805OsuYwM70TDHwh6v2cGOHpQg0AOl5tFNPYTzMtSZBkxKdHeXaM90n9Bx1UA+vheHCCfs++UU16V1gevI5R7/IpYdaklktGSaKM1WneVUbhwm/LNYYISw2fc9H4DwSn9EOdjo345AysDZGl2ttMm5VPSY4+pAdDR5F54LMo9YOoZORHUOmzQzyZySJcXH9v3n7/aVwIDAQAB"
        comment = "DomainKeys Identified Mail (DKIM) key for Google Mail"
      }
      spf = {
        name    = cloudflare_record.apex.name
        value   = "v=spf1 include:_spf.google.com ~all"
        comment = "Sender Policy Framework (SPF) for Google Mail"
      }
    }
  }
}

resource "cloudflare_record" "txt" {
  for_each = local.dns.txt
  zone_id  = cloudflare_zone.primary.id
  name     = each.value.name
  type     = "TXT"
  value    = each.value.value
  comment  = each.value.comment
}

resource "cloudflare_record" "mx" {
  for_each = {
    "aspmx.l.google.com" : 10,
    "alt1.aspmx.l.google.com" : 20,
    "alt2.aspmx.l.google.com" : 20,
    "aspmx2.googlemail.com" : 30,
    "aspmx3.googlemail.com" : 30,
  }
  zone_id  = cloudflare_zone.primary.id
  name     = cloudflare_record.apex.name
  type     = "MX"
  value    = each.key
  priority = each.value
}

resource "cloudflare_page_rule" "redirect_www_to_apex" {
  zone_id = cloudflare_zone.primary.id
  target  = "www.kanzlei-spreewald.de"
  actions {
    forwarding_url {
      url         = "https://kanzlei-spreewald.de"
      status_code = 301
    }
  }
}
